##*Teste Lógico Compasso*

Aplicativos necessários para teste:

1. Java8.
2. Eclipse.
3. Postman.

*Obs.: A versão do Eclipse utilizada foi 2020-03 (4.15.0).*

---

**Cadastrar cidade**

URL: (http://localhost:8080/api/cidade)

Método: POST

```Javascript
{
    "municipio": "AGUDO",
    "ufId": {
        "id": 43
    }
}
```
Retorno:

```Javascript
{
	"id": 7, 
	"mensagem":"Cidade incluida com sucesso."
}
```

Caso a string da cidade já existir na UF enviada o retorno será o get de informações da cidade, conforme abaixo:

```Javascript
[
    {
        "id": 7,
        "municipio": "AGUDO",
        "ufId": {
            "id": 43,
            "uf": "RS"
        }
    }
]
```
---

**Consultar cidade pelo nome**

URL: (http://localhost:8080/api/cidade?nome=AGUDO)

Método: GET

Obs.: Não possui case sensitive (http://localhost:8080/api/cidade?nome=AGUDo)

Retorno:

```Javascript
[
    {
        "id": 3,
        "municipio": "AGUDO",
        "ufId": {
            "id": 43,
            "uf": "RS"
        }
    }
]
```
---

**Consultar cidade pelo estado**

*Exemplo 1:*

URL: (http://localhost:8080/api/cidade?uf=43)

Método: GET

Retorno:

```Javascript
[
    {
        "id": 1,
        "municipio": "ACEGUA",
        "ufId": {
            "id": 43,
            "uf": "RS"
        }
    },
    {
        "id": 2,
        "municipio": "AGUA SANTA",
        "ufId": {
            "id": 43,
            "uf": "RS"
        }
    },
    {
        "id": 3,
        "municipio": "AGUDO",
        "ufId": {
            "id": 43,
            "uf": "RS"
        }
    }
]
```

*Exemplo 2:*

URL: (http://localhost:8080/api/cidade?uf=RS).

Método: GET

Retorno:

```Javascript
[
    {
        "id": 1,
        "municipio": "ACEGUA",
        "ufId": {
            "id": 43,
            "uf": "RS"
        }
    },
    {
        "id": 2,
        "municipio": "AGUA SANTA",
        "ufId": {
            "id": 43,
            "uf": "RS"
        }
    },
    {
        "id": 3,
        "municipio": "AGUDO",
        "ufId": {
            "id": 43,
            "uf": "RS"
        }
    }
]
```
---

**Cadastrar cliente**

URL: (http://localhost:8080/api/cliente)

Método: POST

```Javascript
{
    "nome": "MARIA DA GRACA",
    "sexo": "FEMININO",
    "dataNascimento": "1982-10-08",
    "idade": "29",
    "cidadeId": {
        "id": 1
    }
}
```
Retorno:

```Javascript
{
	"id": 7, 
	"mensagem":"Cliente incluido com sucesso."
}
```
---

**Consultar cliente pelo nome**

URL: (http://localhost:8080/api/cliente?nome=maria)

Método: GET

Retorno:

```Javascript
[
    {
        "id": 7,
        "nome": "MARIA DA GRAXA",
        "sexo": "FEMININO",
        "dataNascimento": "1982-10-08",
        "idade": 29,
        "cidadeId": {
            "id": 1,
            "municipio": "ACEGUA",
            "ufId": {
                "id": 43,
                "uf": "RS"
            }
        }
    }
]
```
---

**Consultar cliente pelo id**

URL: (http://localhost:8080/api/cliente?id=7)

Método: GET

Retorno:

```Javascript
[
    {
        "id": 7,
        "nome": "MARIA DA GRAXA",
        "sexo": "FEMININO",
        "dataNascimento": "1982-10-08",
        "idade": 29,
        "cidadeId": {
            "id": 1,
            "municipio": "ACEGUA",
            "ufId": {
                "id": 43,
                "uf": "RS"
            }
        }
    }
]
```
---

**Alterar nome do cliente pelo id**

URL: (http://localhost:8080/api/cliente?id=7)

Método: PUT


```Javascript
{
    "id":8,
    "nome": "MARIA DA GRAX"
}
```

Retorno de sucesso:

```Javascript
{
	"mensagem":"Cliente atualizado com sucesso."
}
```

Retorno de cliente não encontrado:

```Javascript
{
	"mensagem":"Cliente não encontrado."
}
```

---

**Deletar cliente pelo id**

URL: (http://localhost:8080/api/cliente?id=7)

Método: DELETE

Retorno de sucesso:

```Javascript
{
	"mensagem":"Cliente excluido com sucesso."
}
```

Retorno de cliente não encontrado:

```Javascript
{
	"mensagem":"Cliente não encontrado."
}
```
---