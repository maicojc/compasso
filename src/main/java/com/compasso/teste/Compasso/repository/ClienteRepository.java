package com.compasso.teste.Compasso.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.compasso.teste.Compasso.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

	@Query("select c from Cliente c where nome like %:nome%")
	List<Cliente> findByNomeCliente(@Param("nome") String nome);

	@Query("select c from Cliente c where id =:id")
	List<Cliente> findByCodigo(@Param("id") Long id);

}
