package com.compasso.teste.Compasso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compasso.teste.Compasso.model.UF;

public interface UfRepository extends JpaRepository<UF, Long>{

}
