package com.compasso.teste.Compasso.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.compasso.teste.Compasso.model.Cidade;
import com.compasso.teste.Compasso.model.UF;

public interface CidadeRepository extends JpaRepository<Cidade, Long>{
	
	@Query("select c from Cidade c where municipio =:cidade")
	public List<Cidade> findByNomeCidade(@Param("cidade") String cidade);

	@Query("select c from Cidade c where municipio =:cidade and ufId=:uf")
	public List<Cidade> findByNomeCidadeAndUf(String cidade, UF uf);

	@Query("select c from Cidade c where ufId=:uf")
	public List<Cidade> findByUfId(UF uf);
	
	@Query("select c from " + 
			"Cidade c, " + 
			"UF u " + 
			"where c.ufId = u.id " + 
			"and u.UF=:uf")
	public List<Cidade> findByUfSring(String uf);

}
