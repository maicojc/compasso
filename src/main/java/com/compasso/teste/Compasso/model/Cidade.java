package com.compasso.teste.Compasso.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Cidade")
public class Cidade {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String municipio;
	
	@ManyToOne
	@JoinColumn(name = "id_uf_fk")
	private UF ufId;
	
	@JsonIgnore
	@OneToMany(
			mappedBy = "cidadeId", 
			cascade = CascadeType.ALL,
			orphanRemoval = true
	)
	private List<Cliente> cliente = new ArrayList<Cliente>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public UF getUfId() {
		return ufId;
	}

	public void setUfId(UF ufId) {
		this.ufId = ufId;
	}

	public List<Cliente> getCliente() {
		return cliente;
	}

	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}

}
