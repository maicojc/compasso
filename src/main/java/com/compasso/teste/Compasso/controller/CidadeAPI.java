package com.compasso.teste.Compasso.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.compasso.teste.Compasso.model.Cidade;
import com.compasso.teste.Compasso.repository.CidadeRepository;
import com.compasso.teste.Compasso.repository.UfRepository;

@RestController
@RequestMapping("/api/cidade")
public class CidadeAPI {
	
	@Autowired
	private CidadeRepository repoCid;
	
	@Autowired
	private UfRepository repoUf;
	
	String retornoAPI = "";
	
	@GetMapping(params = {"nome"})
	public ResponseEntity<List<Cidade>> pesquisarNome(@RequestParam String nome)
	{
		List<Cidade> cidade = repoCid.findByNomeCidade(nome.toUpperCase());
		ResponseEntity<List<Cidade>> retorno = null;
		
		if(cidade.isEmpty()) {
			
			retorno = new ResponseEntity<>(HttpStatus.NO_CONTENT);
			
		} else {
			
			retorno = new ResponseEntity<>(cidade, HttpStatus.OK);
		}
		
		System.out.println(retorno);
		
		return retorno;
	}
	
	@GetMapping(params = {"uf"})
	public ResponseEntity<List<Cidade>> pesquisarUF(@RequestParam String uf)
	{
		List<Cidade> cidade = new ArrayList<>();
		
		ResponseEntity<List<Cidade>> retorno = null;
		
		try {
			
			Long ufToLong = Long.parseLong(uf);
			
			cidade = repoCid.findByUfId(repoUf.findById(ufToLong).get());
			
			if(cidade.isEmpty()) {
				
				retorno = new ResponseEntity<>(HttpStatus.NO_CONTENT);
				
			} else {
				
				retorno = new ResponseEntity<>(cidade, HttpStatus.OK);
			}
			
		} catch(Exception e) {
			
			try {
				
				cidade = repoCid.findByUfSring(uf);
				
				if(cidade.isEmpty()) {
				
					retorno = new ResponseEntity<>(HttpStatus.NO_CONTENT);
					
				} else {
					
					retorno = new ResponseEntity<>(cidade, HttpStatus.OK);	
				}
				
			} catch (Exception e2) {
				
				retorno = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			
		}
		
		return retorno;
	}

	@PostMapping
	public ResponseEntity<?> incluir(@RequestBody Cidade cidade, BindingResult result){
		
		ResponseEntity<?> retorno = null;
		
		if(result.hasErrors()) {
			
			List<FieldError> erros = result.getFieldErrors();
			retorno = new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
			
		} else {
			
			List<Cidade> pesquisaCidade = repoCid.findByNomeCidadeAndUf(cidade.getMunicipio().toUpperCase(),repoUf.findById(cidade.getUfId().getId()).get());
			
			if(pesquisaCidade.isEmpty()) {
			
				repoCid.save(cidade);
				
				retornoAPI = "{\"id\": "+cidade.getId().toString()+", \"mensagem\":\"Cidade incluida com sucesso.\"}";
				
				retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CREATED);
			
			} else {
				
				retorno = new ResponseEntity<>(pesquisaCidade,HttpStatus.CONFLICT);
				
			}
		}
		
		return retorno;
	}
	
	@PutMapping
	public ResponseEntity<?> salvar(@RequestBody Cidade cidade, BindingResult result){
		
		ResponseEntity<?> retorno = null;
		
		if(result.hasErrors()) {
			
			List<FieldError> erros = result.getFieldErrors();
			retorno = new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
			
		} else {
			
			repoCid.save(cidade);
			
			retornoAPI = "{\"mensagem\":\"Cidade atualiza com sucesso.\"}";
			
			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CREATED);
		}
		
		return retorno;
	}
	
	@DeleteMapping(params = "id")
	public ResponseEntity<Object> deletar(@RequestParam String id)
	{
		Optional<Cidade> cidade = repoCid.findById(Long.parseLong(id));
		
		if(cidade.isPresent()) {
			
			repoCid.deleteById(Long.parseLong(id));
			
			retornoAPI = "{\"mensagem\":\"Cidade excluida com sucesso.\"}";
			
			return new ResponseEntity<>(retornoAPI,HttpStatus.OK);
			
		} else {
			
			retornoAPI = "{\"mensagem\":\"Cidade não encontrada.\"}";
			
			return new ResponseEntity<>(retornoAPI,HttpStatus.NOT_FOUND);
		}
	}
}
