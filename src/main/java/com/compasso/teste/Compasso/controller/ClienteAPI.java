package com.compasso.teste.Compasso.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.compasso.teste.Compasso.model.Cliente;
import com.compasso.teste.Compasso.repository.ClienteRepository;

@RestController
@RequestMapping("/api/cliente")
public class ClienteAPI {

	@Autowired
	private ClienteRepository repoCli;
	
	String retornoAPI = "";
	
	@GetMapping(params = {"nome"})
	public ResponseEntity<List<Cliente>> pesquisarPorNome(@RequestParam String nome)
	{
		List<Cliente> cliente = repoCli.findByNomeCliente(nome.toUpperCase());
		ResponseEntity<List<Cliente>> retorno = null;
		
		if(cliente.isEmpty()) {
			
			retorno = new ResponseEntity<>(HttpStatus.NO_CONTENT);
			
		} else {
			
			retorno = new ResponseEntity<>(cliente, HttpStatus.OK);
		}
		
		System.out.println(retorno);
		
		return retorno;
	}
	
	@GetMapping(params = {"id"})
	public ResponseEntity<List<Cliente>> pesquisarPorId(@RequestParam String id)
	{
		try {
			
			Long idToLong = Long.parseLong(id);
			
			List<Cliente> cliente = repoCli.findByCodigo(idToLong);
			ResponseEntity<List<Cliente>> retorno = null;
			
			if(cliente.isEmpty()) {
				
				retorno = new ResponseEntity<>(HttpStatus.NO_CONTENT);
				
			} else {
				
				retorno = new ResponseEntity<>(cliente, HttpStatus.OK);
			}
			
			return retorno;
			
		} catch (Exception e) {
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping
	public ResponseEntity<?> incluir(@RequestBody Cliente cliente, BindingResult result){
		
		ResponseEntity<?> retorno = null;
		
		if(result.hasErrors()) {
			
			List<FieldError> erros = result.getFieldErrors();
			retorno = new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
			
		} else {
			
			repoCli.save(cliente);
				
			retornoAPI = "{\"id\": "+cliente.getId().toString()+", \"mensagem\":\"Cliente incluido com sucesso.\"}";
				
			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CREATED);
			
		}
		
		return retorno;
	}
	
	@PutMapping
	public ResponseEntity<?> salvar(@RequestBody Cliente cliente, BindingResult result){
		
		ResponseEntity<?> retorno = null;
		
		if(result.hasErrors()) {
			
			List<FieldError> erros = result.getFieldErrors();
			retorno = new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
			
		} else {
			
			try {
				
				Cliente getCliente = repoCli.findById(cliente.getId()).get();
				
				getCliente.setNome(cliente.getNome());;
				
				repoCli.save(getCliente);
				
				retornoAPI = "{\"mensagem\":\"Cliente atualizado com sucesso.\"}";
				
				retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CREATED);
				
			}catch (Exception e) {
				
				if(e.getMessage() == "No value present") {
					
					retornoAPI = "{\"mensagem\":\"Cliente não encontrado.\"}";
					
					retorno = new ResponseEntity<>(retornoAPI,HttpStatus.NOT_FOUND);
					
				} else {
					
					retornoAPI = "{\"mensagem\":\""+e.getMessage()+"\"}";
					
					retorno = new ResponseEntity<>(retornoAPI,HttpStatus.BAD_REQUEST);
				}
			}
		}
		
		return retorno;
	}
	
	@DeleteMapping(params = "id")
	public ResponseEntity<Object> deletar(@RequestParam String id)
	{
		Optional<Cliente> cidade = repoCli.findById(Long.parseLong(id));
		
		if(cidade.isPresent()) {
			
			repoCli.deleteById(Long.parseLong(id));
			
			retornoAPI = "{\"mensagem\":\"Cliente excluido com sucesso.\"}";
			
			return new ResponseEntity<>(retornoAPI,HttpStatus.OK);
			
		} else {
			
			retornoAPI = "{\"mensagem\":\"Cliente não encontrado.\"}";
			
			return new ResponseEntity<>(retornoAPI,HttpStatus.NOT_FOUND);
		}
	}
}
