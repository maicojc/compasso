package com.compasso.teste.Compasso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.compasso.teste.Compasso.model.Cidade;
import com.compasso.teste.Compasso.model.UF;
import com.compasso.teste.Compasso.repository.CidadeRepository;
import com.compasso.teste.Compasso.repository.UfRepository;

@Component
public class Loading implements CommandLineRunner{
	
	@Autowired
	private UfRepository repoUf;
	
	@Autowired
	private CidadeRepository repoCid;
	
	@Override
	public void run(String... args) throws Exception{
		
		UF uf = new UF();
		
		uf.setId(12L);
		uf.setUF("AC");
		repoUf.save(uf);
		
		uf.setId(27L);
		uf.setUF("AL");
		repoUf.save(uf);
		
		uf.setId(16L);
		uf.setUF("AP");
		repoUf.save(uf);
		
		uf.setId(13L);
		uf.setUF("AM");
		repoUf.save(uf);
		
		uf.setId(29L);
		uf.setUF("BA");
		repoUf.save(uf);
		
		uf.setId(23L);
		uf.setUF("CE");
		repoUf.save(uf);
		
		uf.setId(53L);
		uf.setUF("DF");
		repoUf.save(uf);
		
		uf.setId(32L);
		uf.setUF("ES");
		repoUf.save(uf);
		
		uf.setId(52L);
		uf.setUF("GO");
		repoUf.save(uf);
		
		uf.setId(21L);
		uf.setUF("MA");
		repoUf.save(uf);
		
		uf.setId(51L);
		uf.setUF("MT");
		repoUf.save(uf);
		
		uf.setId(50L);
		uf.setUF("MS");
		repoUf.save(uf);
		
		uf.setId(31L);
		uf.setUF("MG");
		repoUf.save(uf);
		
		uf.setId(15L);
		uf.setUF("PA");
		repoUf.save(uf);
		
		uf.setId(25L);
		uf.setUF("PB");
		repoUf.save(uf);
		
		uf.setId(41L);
		uf.setUF("PR");
		repoUf.save(uf);
		
		uf.setId(26L);
		uf.setUF("PE");
		repoUf.save(uf);
		
		uf.setId(22L);
		uf.setUF("PI");
		repoUf.save(uf);
		
		uf.setId(33L);
		uf.setUF("RJ");
		repoUf.save(uf);
		
		uf.setId(24L);
		uf.setUF("RN");
		repoUf.save(uf);
		
		uf.setId(43L);
		uf.setUF("RS");
		repoUf.save(uf);
		
		uf.setId(11L);
		uf.setUF("RO");
		repoUf.save(uf);
		
		uf.setId(14L);
		uf.setUF("RR");
		repoUf.save(uf);
		
		uf.setId(42L);
		uf.setUF("SC");
		repoUf.save(uf);
		
		uf.setId(35L);
		uf.setUF("SP");
		repoUf.save(uf);
		
		uf.setId(28L);
		uf.setUF("SE");
		repoUf.save(uf);
		
		uf.setId(17L);
		uf.setUF("TO");
		repoUf.save(uf);
		
		Cidade cidade = new Cidade();
		
		cidade.setId(4300034L);
		cidade.setMunicipio("ACEGUA");
		cidade.setUfId(repoUf.findById(43L).get());
		repoCid.save(cidade);
		
		cidade.setId(4300059L);
		cidade.setMunicipio("AGUA SANTA");
		cidade.setUfId(repoUf.findById(43L).get());
		repoCid.save(cidade);
		
		cidade.setId(4300109L);
		cidade.setMunicipio("AGUDO");
		cidade.setUfId(repoUf.findById(43L).get());
		repoCid.save(cidade);
		
		cidade.setId(4300208L);
		cidade.setMunicipio("AJURICABA");
		cidade.setUfId(repoUf.findById(43L).get());
		repoCid.save(cidade);
		
		cidade.setId(4300307L);
		cidade.setMunicipio("ALECRIM");
		cidade.setUfId(repoUf.findById(43L).get());
		repoCid.save(cidade);
		
		cidade.setId(4300406L);
		cidade.setMunicipio("ALEGRETE");
		cidade.setUfId(repoUf.findById(43L).get());
		repoCid.save(cidade);
	}

}
